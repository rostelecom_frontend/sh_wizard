# Виджет визарда подключения камеры

Подключение npm модуля из локальной папки в package.json проекта

```
"dependencies": {
  "vc-camera-wizard": "file:./path_to_folder"
}
```

Использование в реакт компоненте

```
import WizardWidget from 'vc-camera-wizard';


// обработчик нажатия на кнопку Перейти к камере на финальном шаге визарда при успешной регистрации камеры
// и на шаге регистрации по серийному номеру (после ввода серийного номера и нажатии кнопки Подключить происходит
// проверка наличия камеры с таким серийным номером в сторе и переходом на шаг Камера уже подключена)

const to_camera_handler = (uid) => {
    console.info('---> go to camera', uid);
};

// обработчик смены шага виджетом визарда

const step_change_handler = (is_start) => {
    console.info('---> step changed to start', is_start);
};

<WizardWidget
    proxyUrl='/services/vc'
    handleGoToCamera={to_camera_handler}
    handleStepChange={step_change_handler}
    session: PropTypes.shape({
        'X-User-Key': PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        'X-User-Token': PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        'HTTP_HOME_ID': PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })
/>
```


- proxyUrl - префикс пути до прокси УД для вызова api ВК
- handleGoToCamera - обработчик нажатия на кнопку Перейти к камере на финальном шаге визарда при успешной регистрации камеры
- step_change_handler - обработчик смены шага виджетом визарда, возвращает true, если текущйи шаг - стартовый, иначе - false
- session объект с тремя ключами (X-User-Key, X-User-Token, HTTP_HOME_ID) для заголовков в запросах


Если виджет визарда используется внутри модального попапа,
то в обработчики закрытия попапа и перехода на предыдущий шаг визарда
надо добавить эмитирование событий closeWizardWidgetEvent и 'prevStepWizardWidgetEvent' соответственно,
визард слушает эти события и сбросит состояние визарда к начальному шагу при закрытии
и покажет предыдущий шаг, когда это возможно

Пример использования эмиттера событий визарда:

```
import CameraWizard, { wizardEmitter, CLOSE_WIZARD_WIDGET_EVENT, PREV_STEP_WIZARD_WIDGET_EVENT } from 'vc-camera-wizard';
```

- wizardEmitter - экземпляр EventEmitter
- CLOSE_WIZARD_WIDGET_EVENT - константа названия события закрытия визарда (сбросит визард на начальный шаг)
- PREV_STEP_WIZARD_WIDGET_EVENT - константа названия события перехода на предыдущий шаг визарда

Для эмитирования события закрытия визарда:

```
wizardEmitter.emit(CLOSE_WIZARD_WIDGET_EVENT);
```

Для эмитирования события перехода на предыдущий шаг визарда:

```
wizardEmitter.emit(PREV_STEP_WIZARD_WIDGET_EVENT);
```


### Регистрация камеры по QR-коду (при установленном wifi-соединении)

- для успешного формирования qr-кода, необходимо наличие в верстке страницы установленной меты для stage и demo окружения (НЕ production)

```
<meta name="env" content=“development”>
```

- при подготовке qr-кода визард получает значение переменной и добавляет его в код

